from math import *
from random import *

dataset=[[0,0],[0,0],[0,0],[0,1],[0,1],[0,1],[1,0],[1,0],[1,0],[1,1],[1,1],[1,1]]
target=[[0],[0],[0],[0],[0],[0],[0],[0],[0],[1],[1],[1]]

dcount=len(dataset[0])
hneuron=int(ceil(sqrt(len(dataset))))
oneuron=len(target[0])

w1=[]
w2=[]
b1=uniform(-2.4/len(dataset),2.4/len(dataset))
b2=uniform(-2.4/len(dataset),2.4/len(dataset))


for a in range(hneuron):
	weight=[]
	for b in range(dcount):
		weight.append(uniform(-2.4/len(dataset),2.4/len(dataset)))
	w1.append(weight)

for a in range(oneuron):
	weight=[]
	for b in range(hneuron):
		weight.append(uniform(-2.4/len(dataset),2.4/len(dataset)))
	w2.append(weight)

maxmse=random()*0.1
mse=maxmse+1
maxeppoch=500
eppoch=1
lr=0.1

while (eppoch<=maxeppoch)and(mse>=maxmse):
	mse=0
	for a in range(len(dataset)):
		# activation
		p=dataset[a]
		t=target[a]	
		a1=[]
		for i in range(hneuron):
			v=0
			for j in range(dcount):
				v+=float(w1[i][j])*p[j]-b1
			a1.append(1/(1+exp(-v)))

		a2=[]
		for i in range(oneuron):
			v=0
			for j in range(hneuron):
				v+=w2[i][j]*a1[j]-b2
			a2.append(1/(1+exp(-v)))

		e=[]
		for i in range(oneuron):
			e.append(float(t[i])-a2[i])

		for i in range(oneuron):
			mse+=(e[i]/2)**2


		# update weights
		d2=[]
		for i in range(oneuron):
			d2.append(a2[i]*(1-a2[i])*e[i])
		
		d1=[]
		for i in range(hneuron):
			v=0
			for j in range(oneuron):
				v+=d2[j]*w2[j][i]
			d1.append(a1[i]*(1-a1[i])*v)

		dw2=[]
		for i in range(oneuron):
			v=[]
			for j in range(hneuron):
				v.append(lr*d2[i]*a1[j])
			dw2.append(v)


		dw1=[]
		for i in range(hneuron):
			v=[]
			for j in range(dcount):
				v.append(lr*p[j]*d1[i])
			dw1.append(v)

		for i in range(oneuron):
			for j in range(hneuron):
				w2[i][j]+=dw2[i][j]

		for i in range(hneuron):
			for j in range(dcount):
				w1[i][j]+=dw1[i][j]

	totalmse=mse/dcount
	print totalmse
	eppoch+=1

jmlbenar=0
for i in range(len(dataset)):
	a1=[]
	a2=[]
	p=dataset[i]
	t=target[i]
	kelas=[]

	for a in range(hneuron):
		v=0
		for b in range(dcount):
			v+=w1[a][b]*p[b]
		a1.append(1/(1+exp(-v)))

	for a in range(oneuron):
		v=0
		for b in range(hneuron):
			v+=w2[a][b]*a1[b]
		a2.append(1/(1+exp(-v)))

	for a in range(len(a2)):
		if a2[a]<0.5:
			kelas.append(0)
		else:
			kelas.append(1)

	print kelas

print [w1,w2,b1,b2]