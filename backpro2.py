import csv
import random
import math
from decimal import *

# training
def training(filetraining):
	dataset=[]
	output=[]
	with open(filetraining,'rb') as f:
		reader=csv.reader(f)
		for row in reader:
			if row[4]=='Iris-setosa':
				row[4]=0
			elif row[4]=='Iris-versicolor':
				row[4]=1
			dataset.append([row[0],row[1],row[2],row[3]])
			output.append(row[4])

	maxse=random.random()*0.1
	seepoch=maxse+1
	lr = 0.1
	maxepoch = 100000

	w1=[]
	w2=[]

	bias1=random.random()
	bias2=random.random()
	field=int(math.ceil(math.sqrt(len(dataset[0]))))
	for i in range(field):
		w1.append([random.uniform(-0.5,0.5),random.uniform(-0.5,0.5),random.uniform(-0.5,0.5),random.uniform(-0.5,0.5)])

	for i in range(len(w1)):
		w2.append(random.uniform(-0.5,0.5))
	epoch=1
	jml=0
	while (epoch<=maxepoch)and(seepoch>=maxse)and(jml<95):
		seepoch=0
		jmlbenar=0
		for i in range(len(dataset)):
			# perhitungan maju
			currdata=dataset[i]
			t=output[i]
			a1=[]

			for a in range(len(w1)):
				v1=0
				for b in range(len(currdata)):
					v1+=(float(currdata[b])*w1[a][b])+bias1
				a1.append(1/(1+math.exp(-v1)))

			for a in range(len(a1)):
				v=0
				v+=(a1[a]*w2[a]+bias2)
			a2=1/(1+math.exp(-v))

			e=float(t)-a2
			seepoch=seepoch+(e**2)

			#perhitungan mundur
			d2=a2*(1-a2)*e
			dW2=0
			db2=0
			for a in range(len(a1)):
				dW2+=(lr*d2*a1[a])
			db2+=(lr*d2)

			for a in range(len(w2)):
				w2[a]+=dW2

			bias2+=db2

			d1=[]
			dW1=0
			db1=0

			for a in range(len(a1)):
				d1.append(a1[a]*(1-a1[a])*w2[a]*d2)

			for a in range(len(d1)):
				for b in range(len(currdata)):
					dW1+=(lr*d1[a]*float(currdata[b]))
				db1+=(lr*d1[a])

			for a in range(len(w1)):
				for b in range(len(w1[a])):
					w1[a][b]+=dW1

			bias1=bias1+db1

		for i in range(len(dataset)):
			currdata=dataset[i]

			a1=[]
			
			for a in range(len(w1)):
				v1=0
				for b in range(len(currdata)):
					v1+=(float(currdata[b])*w1[a][b])+bias1
				a1.append(1/(1+math.exp(-v1)))

			for a in range(len(a1)):
				v=0
				v+=(a1[a]*w2[a]+bias2)
			a2=1/(1+math.exp(-v))

			if a2<0.5:
				kelas=0
			else:
				kelas=1

			if (int(kelas)==int(output[i])):
				jmlbenar+=1

		jml=float(jmlbenar)/float(len(dataset))*100.0
		print 'Akurasi Training ',epoch,': ',jml,'%'

		mse=seepoch/len(dataset)
		epoch+=1

	print 'great accuracy? ',jml>=95
	print 'min mse? ',seepoch<maxse
	return [w1,w2,bias1,bias2]

def testing(filetesting,data):
	dataset=[]
	output=[]
	jmlbenar=0
	w1=data[0]
	w2=data[1]
	bias1=data[2]
	bias2=data[3]
	with open(filetesting,'rb') as f:
		reader=csv.reader(f)
		for row in reader:
			if row[4]=='Iris-setosa':
				row[4]=0
			elif row[4]=='Iris-versicolor':
				row[4]=1
			dataset.append([row[0],row[1],row[2],row[3]])
			output.append(row[4])


	for i in range(len(dataset)):
		currdata=dataset[i]

		a1=[]
		
		for a in range(len(w1)):
			v1=0
			for b in range(len(currdata)):
				v1+=(float(currdata[b])*w1[a][b])+bias1
			a1.append(1/(1+math.exp(-v1)))

		for a in range(len(a1)):
			v=0
			v+=(a1[a]*w2[a]+bias2)
		a2=1/(1+math.exp(-v))

		if a2<0.5:
			kelas=0
		else:
			kelas=1

		if (int(kelas)==int(output[i])):
			jmlbenar+=1

	jml=float(jmlbenar)/float(len(dataset))*100.0
	print 'Akurasi Testing : ',jml,'%'

def coba(currdata,data):
	a1=[]
	w1=data[0]
	w2=data[1]
	bias1=data[2]
	bias2=data[3]
	for a in range(len(w1)):
		v1=0
		for b in range(len(currdata)):
			v1+=(float(currdata[b])*w1[a][b])+bias1
		a1.append(1/(1+math.exp(-v1)))

	for a in range(len(a1)):
		v=0
		v+=(a1[a]*w2[a]+bias2)
	a2=1/(1+math.exp(-v))

	if a2<0.5:
		kelas='Iris-setosa'
	else:
		kelas='Iris-versicolor'

	print "Bunga dengan ciri2 tersebut adalah jenis : ",kelas


data=training('iris-training.csv')
testing('iris-testing.csv',data)
coba([4.7,3.2,1.3,0.2],data)
