import csv
import random
import math
import pylab

# training
def training(filetraining):
	dataset=[]
	output=[]

	#open file
	with open(filetraining,'rb') as f:
		reader=csv.reader(f)
		for row in reader:
			if row[4]=='Iris-setosa':
				row[4]=[0,0,1]
			elif row[4]=='Iris-versicolor':
				row[4]=[0,1,0]
			else:
				row[4]=[1,0,0]
			dataset.append([row[0],row[1],row[2],row[3]])
			output.append(row[4])


	variable=len(dataset[0])
	hnode=int(math.ceil(math.sqrt(len(dataset))))
	onode=len(output[0])

	#define weight each neuron
	w1=[]
	w2=[]
	bias1=random.uniform(-2.4/len(dataset),2.4/len(dataset))
	bias2=random.uniform(-2.4/len(dataset),2.4/len(dataset))
	for i in range(hnode):
		neuron=[]
		for j in range(variable):
			neuron.append(random.uniform(-2.4/len(dataset),2.4/len(dataset)))
		w1.append(neuron)

	for i in range(onode):
		neuron=[]
		for j in range(hnode):
			neuron.append(random.uniform(-2.4/len(dataset),2.4/len(dataset)))
		w2.append(neuron)
	
	#define training range

	maxse=random.random()*0.01
	seepoch=maxse+1
	lr = 0.05
	maxepoch = 100

	epoch=1
	jml=0
	ttot=[]
	a1tot=[]
	eparr=[]
	f1=pylab.figure()
	ax1 = f1.add_subplot(111)
	while (epoch<=maxepoch):
		print "==========================================="
		print "eppoch ke-",epoch
		seepoch=0
		jmlbenar=0
		for i in range(len(dataset)):
			# perhitungan maju
			currdata=dataset[i]
			t=output[i]
			a1=[]
			a2=[]
			e=[]

			for a in range(hnode):
				v1=0
				for b in range(variable):
					v1+=(float(currdata[b])*w1[a][b])+bias1
				a1.append(1/(1+math.exp(-v1)))

			for a in range(onode):
				v2=0
				for b in range(hnode):
					v2+=a1[b]*w2[a][b]+bias2
				a2.append(1/(1+math.exp(-v2)))

			for a in range(onode):
				e.append(float(t[a])-a2[a])

			v=0

			for a in range(onode):
				v+=e[a]
			seepoch+=(v**2/2)

			#perhitungan mundur
			d2=[]
			for a in range(onode):
				d2.append(a2[a]*(1-a2[a])*e[a])
			
			d1=[]
			for a in range(hnode):
				v=0
				for b in range(onode):
					v+=d2[b]*w2[b][a]
				d1.append(a1[a]*(1-a1[a])*v)

			dw2=[]
			for a in range(onode):
				v=[]
				for b in range(hnode):
					v.append(lr*d2[a]*a1[b])
				dw2.append(v)


			dw1=[]
			for a in range(hnode):
				v=[]
				for b in range(variable):
					v.append(lr*float(currdata[b])*d1[a])
				dw1.append(v)

			for a in range(onode):
				for b in range(hnode):
					w2[a][b]+=dw2[a][b]

			for a in range(hnode):
				for b in range(variable):
					w1[a][b]+=dw1[a][b]

		mse=seepoch/len(dataset)
		ax1.bar(epoch,mse,color='g')
		print mse
		epoch+=1

	ax1.set_title('MSE Graph')
	ax1.set_xlabel('Eppoch')
	ax1.set_ylabel('MSE')
	print 'min mse? ',seepoch<maxse,'\n'
	print [w1,w2,bias1,bias2]
	return [w1,w2,bias1,bias2]

def testing(filetesting,data):
	dataset=[]
	output=[]
	sepal_w=[]
	sepal_l=[]
	petal_w=[]
	petal_l=[]
	sepal_w.append([])
	sepal_w.append([])
	sepal_w.append([])
	sepal_w.append([])
	sepal_w.append([])
	sepal_l.append([])
	sepal_l.append([])
	sepal_l.append([])
	sepal_l.append([])
	sepal_l.append([])
	petal_l.append([])
	petal_l.append([])
	petal_l.append([])
	petal_l.append([])
	petal_l.append([])
	petal_w.append([])
	petal_w.append([])
	petal_w.append([])
	petal_w.append([])
	petal_w.append([])
	jmlbenar=0
	w1=data[0]
	w2=data[1]
	bias1=data[2]
	bias2=data[3]
	with open(filetesting,'rb') as f:
		reader=csv.reader(f)
		for row in reader:
			if row[4]=='Iris-setosa':
				row[4]=[0,0,1]
			elif row[4]=='Iris-versicolor':
				row[4]=[0,1,0]
			else:
				row[4]=[1,0,0]
			dataset.append([row[0],row[1],row[2],row[3]])
			output.append(row[4])

	variable=len(dataset[0])
	hnode=len(w1)
	onode=len(w2)


	for i in range(len(dataset)):
		currdata=dataset[i]

		a1=[]
		a2=[]
		kelas=[]

		
		for a in range(hnode):
			v1=0
			for b in range(variable):
				v1+=(float(currdata[b])*w1[a][b])+bias1
			a1.append(1/(1+math.exp(-v1)))

		for a in range(onode):
			v2=0
			for b in range(hnode):
				v2+=a1[b]*w2[a][b]+bias2
			a2.append(1/(1+math.exp(-v2)))

		if a2[0]<0.5:
			kelas.append(0)
		else:
			kelas.append(1)

		if a2[1]<0.5:
			kelas.append(0)
		else:
			kelas.append(1)

		if a2[2]<0.5:
			kelas.append(0)
		else:
			kelas.append(1)

		if kelas==[0,0,1]:
			sepal_l[0].append(float(currdata[0]))
			sepal_w[0].append(float(currdata[1]))
			petal_l[0].append(float(currdata[2]))
			petal_w[0].append(float(currdata[3]))
		elif kelas==[0,1,0]:
			sepal_l[1].append(float(currdata[0]))
			sepal_w[1].append(float(currdata[1]))
			petal_l[1].append(float(currdata[2]))
			petal_w[1].append(float(currdata[3]))
		elif kelas==[1,0,0]:
			sepal_l[2].append(float(currdata[0]))
			sepal_w[2].append(float(currdata[1]))
			petal_l[2].append(float(currdata[2]))
			petal_w[2].append(float(currdata[3]))
		else:
			sepal_l[3].append(float(currdata[0]))
			sepal_w[3].append(float(currdata[1]))
			petal_l[3].append(float(currdata[2]))
			petal_w[3].append(float(currdata[3]))

		if (kelas!=output[i]):
			sepal_l[4].append(float(currdata[0]))
			sepal_w[4].append(float(currdata[1]))
			petal_l[4].append(float(currdata[2]))
			petal_w[4].append(float(currdata[3]))

		if (kelas==output[i]):
			jmlbenar+=1

	f2=pylab.figure()
	ax2 = f2.add_subplot(211)
	ax2.set_title('Sepal Classification')
	ax2.set_xlabel('Lenght')
	ax2.set_ylabel('Width')
	ax2.plot(sepal_l[0],sepal_w[0],'ro', label='Iris-setosa')
	ax2.plot(sepal_l[1],sepal_w[1],'go', label='Iris-versicolor')
	ax2.plot(sepal_l[2],sepal_w[2],'bo', label='Iris-virginica')
	ax2.plot(sepal_l[3],sepal_w[3],'x')
	ax2.set_ylim(2,4.5)
	ax2.set_xlim(4,8)
	ax2.legend()
	_ax2 = f2.add_subplot(212)
	_ax2.set_xlabel('Lenght')
	_ax2.set_ylabel('Width')
	_ax2.plot(sepal_l[4],sepal_w[4],'x', label='False Class')
	_ax2.set_ylim(2,4.5)
	_ax2.set_xlim(4,8)
	_ax2.legend(loc='upper left')


	f3=pylab.figure()
	ax3 = f3.add_subplot(211)
	ax3.set_title('Petal Classification')
	ax3.set_xlabel('Lenght')
	ax3.set_ylabel('Width')
	ax3.plot(petal_l[0],petal_w[0],'ro', label='Iris-setosa')
	ax3.plot(petal_l[1],petal_w[1],'go', label='Iris-versicolor')
	ax3.plot(petal_l[2],petal_w[2],'bo', label='Iris-virginica')
	ax3.plot(petal_l[3],petal_w[3],'x')
	ax3.set_ylim(0,2.7)
	ax3.set_xlim(1,7)
	ax3.legend(loc='lower right')
	_ax3 = f3.add_subplot(212)
	_ax3.set_xlabel('Lenght')
	_ax3.set_ylabel('Width')
	_ax3.plot(petal_l[4],petal_w[4],'x', label='False Class')
	_ax3.set_ylim(0,2.7)
	_ax3.set_xlim(1,7)
	_ax3.legend(loc='upper left')

	jml=float(jmlbenar)/float(len(dataset))*100.0
	print 'Akurasi Testing: ',jml,'%\n'

def coba(currdata,data):
	a1=[]
	a2=[]
	w1=data[0]
	w2=data[1]
	bias1=data[2]
	bias2=data[3]
	kelas=[]
	variable=len(currdata)
	hnode=len(w1)
	onode=len(w2)

		
	for a in range(hnode):
		v1=0
		for b in range(variable):
			v1+=(float(currdata[b])*w1[a][b])+bias1
		a1.append(1/(1+math.exp(-v1)))

	for a in range(onode):
		v2=0
		for b in range(hnode):
			v2+=a1[b]*w2[a][b]+bias2
		a2.append(1/(1+math.exp(-v2)))
	
	if a2[0]<0.5:
		kelas.append(0)
	else:
		kelas.append(1)

	if a2[1]<0.5:
		kelas.append(0)
	else:
		kelas.append(1)

	if kelas==[0,0]:
		kelas='Iris-setosa'
	elif kelas==[0,1]:
		kelas='Iris-versicolor'
	elif kelas==[1,0]:
		kelas='Iris-virginica'
	else:
		kelas='unidentified'

	print "Bunga dengan ciri2 tersebut adalah jenis : ",kelas


gooddata=[[[0.4656495018749205, 1.4447638049266465, -2.359395028818353, -1.0973831763827575], [0.379938701705646, 1.164197678158336, -1.9706927153109521, -0.9071707708294868], [0.4164775598734104, 1.2996380164424182, -2.1578234624175434, -0.9825111669433282], [0.4967641641007391, 0.9344459745698579, -1.4528797862113947, -0.8176121705785364], [-2.9664459687117453, -3.191571574363223, 4.180099871768844, 5.09561307978198], [-2.2143123594964993, -2.209275018797647, 3.0712428516160943, 3.801531377433851], [-0.007019336628737791, -0.39205973909707187, 0.7831492392091339, 0.34177396610317956], [-0.34739259869410877, -1.1928844113730555, 2.0108613538982487, 0.9371067486231124], [3.167556598212702, 3.5037158229689944, -4.493150314218132, -5.497179250503587], [0.24526776903369074, 0.16157657565317263, -0.08750763248246003, -0.08913901071225946], [-0.08449663674367103, -0.4208829318616681, 0.7790036806531211, 0.37002495294192916], [1.6102190662371203, 1.6987939250198043, -2.3110220787663396, -2.94501167489513]], [[-1.217281208336071, -1.106337845462832, -1.1147718164345661, -2.1148024570559247, 3.2591869595317946, 2.544687030148977, -0.12840950404111892, -0.002840531533150806, -4.571488959560961, -0.47914939945417806, 0.02773430628025802, -3.524206125830421], [-3.5152050969912, -2.442228993103506, -2.9103642800913967, -1.47960666273391, -4.295156752731622, -2.925382111201054, 0.2456249971678828, 2.34461622191638, 4.246455075648776, -0.48216460399652145, 0.2264443923942441, 1.3523791036921526], [1.8165557652810544, 1.234978781429174, 1.4649462777513995, 1.4434085292373506, -1.3805337503700648, -1.4279479200921503, -1.5803079759221386, -2.5814328467722927, 0.30248476743645153, -0.5434088049715304, -1.593061680181776, 0.7515939414348357]], -0.0061185995649065995, -0.0008904328262322892]
data=training('irisfull-training.csv')
testing('irisfull-training.csv',data)
coba([6.0,3.0,4.8,1.8],data)
pylab.show()